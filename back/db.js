import sqlite from "sqlite";
import SQL from "sql-template-strings";

const initializeDatabase = async () => {
  const db = await sqlite.open("./db.sqlite");

  const createContact = async props => {
    const {
      type,
      amount,
      start_date,
      end_date,
      repeat,
      currency,
      description
    } = props;
    const result = await db.run(
      SQL`INSERT INTO wallet (type, amount , start_date, end_date,repeat,currency , description) VALUES (${type}, ${amount},${start_date},${end_date},${repeat},${currency},${description})`
    );

    const id = result.stmt.lastID;
    return id;
  };

  const createCategory = async props => {
    const { title } = props;
    const result = await db.run(
      SQL`INSERT INTO category (title) VALUES (${title})`
    );
    const id = result.stmt.lastID;
    return id;
  };

  const deleteContact = async id => {
    const result = await db.run(
      SQL`DELETE FROM wallet WHERE transaction_id = ${id}`
    );
    if (result.stmt.changes === 0) {
      return false;
    }
    return true;
  };

  const deleteCategory = async id => {
    const result = await db.run(
      SQL`DELETE FROM category WHERE category_id = ${id}`
    );
    if (result.stmt.changes === 0) {
      return false;
    }
    return true;
  };

  const updateCategory = async (id, props) => {
    const { title } = props;
    const result = await db.run(
      SQL`UPDATE category SET title=${title} WHERE category_id = ${id}`
    );
    if (result.stmt.changes === 0) {
      return false;
    }
    return true;
  };

  const updateContact = async (id, props) => {
    const {
      type,
      amount,
      start_date,
      end_date,
      repeat,
      currency,
      description
    } = props;
    const result = await db.run(
      SQL`UPDATE articles SET
      type=       ${type},
      amount=     ${amount},
      start_date= ${start_date},
      end_date=   ${end_date},
      repeat=     ${repeat},
      currency=   ${currency},
      description=${description}


      WHERE transaction_id = ${id}`
    );
    if (result.stmt.changes === 0) {
      return false;
    }
    return true;
  };

  const getContact = async id => {
    const contactsList = await db.all(
      SQL`SELECT transaction_id AS id, type, amount , start_date, end_date,repeat,currency , description FROM wallet WHERE transaction_id = ${id}`
    );
    const contact = contactsList[0];
    return contact;
  };

  const getCategory = async id => {
    const contactsList = await db.all(
      SQL`SELECT category_id AS id, title FROM category WHERE category_id = ${id}`
    );
    const contact = contactsList[0];
    return contact;
  };

  const getCategoryList = async orderBy => {
    let statement = `SELECT category_id AS id, title FROM category`;
    switch (orderBy) {
      case "title":
        statement += ` ORDER BY title`;
        break;

      default:
        break;
    }
    const rows = await db.all(statement);
    return rows;
  };

  const getContactsList = async orderBy => {
    let statement = `SELECT transaction_id AS id, type, amount , start_date, end_date,repeat,currency , description FROM wallet`;
    switch (orderBy) {
      case "type":
        statement += ` ORDER BY type`;
        break;
      case "amount":
        statement += ` ORDER BY amount`;
        break;
      case "start_date":
        statement += ` ORDER BY start_date`;
        break;
      case "end_date":
        statement += ` ORDER BY end_date`;
        break;

      case "repeat":
        statement += `ORDER BY repeat`;
        break;
      case "currency":
        statement += ` ORDER BY currency`;
        break;

      case "description ":
        statement += ` ORDER BY description `;
        break;

      default:
        break;
    }
    const rows = await db.all(statement);
    return rows;
  };

  const controller = {
    createContact,
    deleteContact,
    updateContact,
    getContact,
    getContactsList,
    createCategory,
    deleteCategory,
    updateCategory,
    getCategory,
    getCategoryList
  };
  return controller;
};

export default initializeDatabase;
