// back/src/app.js
import express from "express";
import helmet from "helmet";
import cookieParser from "cookie-parser"; // parses cookies
import session from "express-session"; // parses sessions
import favicon from "serve-favicon"; // serves favicon
import cors from "cors"; // allows cross-domain requests
import createError from "http-errors"; // better JS errors
import path from "path";
import helmet from "helmet";

import initializeDatabase from "./db";

import { isLoggedIn } from "./src/auth";

const app = express(); // create a new app

const IS_PRODUCTION = app.get("env") === "production";

if (IS_PRODUCTION) {
  app.set("trust proxy", 1); // secures the app if it is running behind Nginx/Apache/similar
}

app.use(cors()); // allows cross domain requests
app.use(express.json()); // allows POST requests with JSON
app.use(express.urlencoded({ extended: false })); // allows POST requests with GET-like parameters
app.use(cookieParser()); // Parses cookies
app.use(favicon(path.join(__dirname, "./public", "favicon.ico"))); // <-- location of your favicon
app.use(express.static(path.join(__dirname, "../public"))); // <-- location of your public dir

app.use(
  session({
    // handles sessions
    secret: "keyboard cat", // <-- this should be a secret phrase
    cookie: { secure: IS_PRODUCTION }, // <-- secure only in production
    resave: true,
    saveUninitialized: true
  })
);

const start = async () => {
  const controller = await initializeDatabase();

  app.get("/", (req, res, next) => res.send("ok"));

  // CREATE
  app.get("/admin/new", async (req, res, next) => {
    const {
      type,
      amount,
      start_date,
      end_date,
      repeat,
      currency,
      description
    } = req.query;
    const result = await controller.createContact({
      type,
      amount,
      start_date,
      end_date,
      repeat,
      currency,
      description
    });
    res.json({ success: true, result });
  });

  app.get("/admin/newCategory", async (req, res, next) => {
    const { title } = req.query;
    const result = await controller.createCategory({
      title
    });
    res.json({ success: true, result });
  });

  // READ
  app.get("/admin/get/:id", async (req, res, next) => {
    const { id } = req.params;
    const contact = await controller.getContact(id);
    res.json({ success: true, result: contact });
  });

  app.get("/admin/getCategory/:id", async (req, res, next) => {
    const { id } = req.params;
    const contact = await controller.getCategory(id);
    res.json({ success: true, result: contact });
  });

  // DELETE
  app.get("/admin/delete/:id", async (req, res, next) => {
    const { id } = req.params;
    const result = await controller.deleteContact(id);
    res.json({ success: true, result });
  });

  app.get("/admin/deleteCategory/:id", async (req, res, next) => {
    const { id } = req.params;
    const result = await controller.deleteCategory(id);
    res.json({ success: true, result });
  });

  // UPDATE
  app.get("/admin/update/:id", async (req, res, next) => {
    const { id } = req.params;
    const {
      type,
      amount,
      start_date,
      end_date,
      repeat,
      currency,
      description
    } = req.query;
    const result = await controller.updateContact(id, {
      type,
      amount,
      start_date,
      end_date,
      repeat,
      currency,
      description
    });
    res.json({ success: true, result });
  });

  app.get("/admin/updateCategory/:id", async (req, res, next) => {
    const { id } = req.params;
    const { title } = req.query;
    const result = await controller.updateCategory(id, {
      title
    });
    res.json({ success: true, result });
  });

  // LIST
  app.get("/admin/listContact", async (req, res, next) => {
    const { order } = req.query;
    const contacts = await controller.getContactsList(order);
    res.json({ success: true, result: contacts });
  });

  app.get("/admin/listCategory", async (req, res, next) => {
    const { order } = req.query;
    const contacts = await controller.getCategoryList(order);
    res.json({ success: true, result: contacts });
  });
  app.get("/mypage", isLoggedIn, (req, res) => {
    const username = req.user.name;
    res.send({
      success: true,
      result: "ok, user " + username + " has access to this page"
    });
  });
  // app.get("/login", authenticateUser);
  // app.get("/logout", logout);
  // app.get("/mypage", isLoggedIn, (req, res) => {
  //   const username = req.user.username;
  //   res.send({
  //     success: true,
  //     result: "ok, user " + username + " has access to this page"
  //   });
  // });

  // ERROR
  app.use((err, req, res, next) => {
    console.error(err);
    const message = err.message;
    res.status(500).json({ success: false, message });
  });

  app.listen(8080, () => console.log("server listening on port 8080"));
};

start();
