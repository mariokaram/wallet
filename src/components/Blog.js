import React, { Component } from "react";
import { Link } from "react-router-dom";

class Blog extends Component {
  state = {
    contacts_list: [],
    category_list: [],
    type: "",
    amount: null,
    start_date: "",
    end_date: "",
    repeat: "",
    currency: "",
    description: "",
    title: ""
  };

  getCategoryList = async () => {
    const response = await fetch("//localhost:8080/admin/listCategory");
    const answer = await response.json();
    if (answer.success) {
      const category_list = answer.result;
      this.setState({ category_list });
    } else {
      const error_message = answer.message;
      this.setState({ error_message });
    }
  };
  componentDidMount() {
    this.getCategoryList();
  }

  render() {
    const { category_list } = this.state;

    return (
      <div>
        <div className="Blog">
          <nav className="navbar navbar-default navbar-fixed-top">
            <div className="container">
              <div className="navbar-header">
                <div
                  className="navbar-toggle collapsed"
                  data-toggle="collapse"
                  data-target="#main-menu"
                >
                  <span className="bar1" />
                  <span className="bar2" />
                  <span className="bar3" />
                </div>
                <a className="navbar-brand top" href="#">
                  <img src="img/logo.png" alt="" />
                </a>
              </div>

              <div className="collapse navbar-collapse" id="main-menu">
                <ul className="nav navbar-nav navbar-right">
                  <li>
                    <Link to="/Home">Home</Link>
                  </li>
                </ul>
              </div>
            </div>
          </nav>
        </div>
        <div>
          <br />
          <br />
          <br />

          <const id="blog">
            <div className="container">
              <div className="row">
                <div className="col-lg-6 col-lg-offset-3 text-center">
                  <h2>
                    <span className="ion-minus" />
                    Wallet
                    <span className="ion-minus" />
                  </h2>

                  <br />
                </div>
              </div>

              <div className="row">
                <div
                  id="slider"
                  className="carousel slide"
                  data-ride="carousel"
                >
                  <ol className="carousel-indicators">
                    <li
                      data-target="#slider"
                      data-slide-to="0"
                      className="active"
                    />
                    <li data-target="#slider" data-slide-to="1" />
                  </ol>

                  <div className="carousel-inner">
                    <div className="item active">
                      <div className="container">
                        <div className="row">
                          {category_list.map(article => {
                            return (
                              <div className="col-md-4 col-xs-12">
                                <div className="card text-center">
                                  <div className="card-block">
                                    <h4 className="card-title">
                                      {article.title}
                                    </h4>
                                  </div>
                                </div>
                              </div>
                            );
                          })}
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </const>
        </div>
      </div>
    );
  }
}
export default Blog;
