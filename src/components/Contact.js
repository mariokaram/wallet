import React from "react";

export default class Contact extends React.Component {
  state = {
    editMode: false
  };
  toggleEditMode = () => {
    const editMode = !this.state.editMode;
    this.setState({ editMode });
  };

  renderViewMode() {
    const {
      id,
      article_title,
      article_image,
      article_text,
      article_date,
      deleteArticle,
      formats
    } = this.props;
    return (
      <tbody>
        <tr>
          <td>{article_title}</td>
          <td>
            <img className="kk" src={article_image} />
          </td>
          <td>
            <p>{article_text}</p>
          </td>

          <td>
            <p>{article_date}</p>
          </td>
          <td>
            <button
              onClick={() => {
                if (
                  window.confirm(
                    `Are you sure you wish to delete this article from your website ?`
                  )
                )
                  deleteArticle(id);
              }}
            >
              x
            </button>
            <button onClick={this.toggleEditMode}>edit</button>
          </td>
        </tr>
      </tbody>
    );
  }
  renderEditMode() {
    const {
      article_title,
      article_image,
      article_text,
      article_date,
      formats
    } = this.props;
    return (
      <tbody>
        <tr>
          <td>
            <form
              className="third"
              onSubmit={this.onSubmits}
              onReset={this.toggleEditMode}
            >
              <input
                type="text"
                placeholder="title"
                name="title"
                defaultValue={article_title}
              />
              <input
                type="text"
                placeholder="description"
                name="text"
                defaultValue={article_text}
              />
              <input type="date" name="date" defaultValue={article_date} />

              <input type="file" name="image" defaultValue={formats} />

              <div>
                <input type="submit" value="ok" />
                <input type="reset" value="cancel" className="button" />
              </div>
            </form>
          </td>
        </tr>
      </tbody>
    );
  }
  onSubmits = evt => {
    // stop the page from refreshing

    // target the form
    const form = evt.target;
    // extract the two inputs from the form
    const title = form.title;
    const text = form.text;
    const date = form.date;
    const image = form.image;

    // extract the values
    const article_title = title.value;
    const article_text = text.value;
    const article_date = date.value;
    const article_image = `./images/${image.files[0].name}`;
    // get the id and the update function from the props
    const { id, updateArticle } = this.props;
    // run the update contact function
    updateArticle(id, {
      article_text,
      article_title,
      article_image,
      article_date
    });

    // toggle back view mode

    // this.toggleEditMode();
  };
  render() {
    const { editMode } = this.state;
    if (editMode) {
      return this.renderEditMode();
    } else {
      return this.renderViewMode();
    }
  }
}
