import React from "react";
import {
  Collapse,
  Navbar,
  NavbarToggler,
  NavbarBrand,
  Nav,
  NavItem,
  NavLink,
  UncontrolledDropdown,
  DropdownToggle,
  DropdownMenu,
  DropdownItem,
  Badge
} from "reactstrap";

// class Header extends React.Component {
//   constructor(props) {
//     super(props);

//     this.toggle = this.toggle.bind(this);
//     this.state = {
//       isOpen: false
//     };
//   }
//   toggle() {
//     this.setState({
//       isOpen: !this.state.isOpen
//     });
//   }
//   render() {
//     return (
//       <div>
//         <Navbar color="light" light expand="md">
//           <NavbarBrand href="/">
//             <h1>Income</h1>
//           </NavbarBrand>
//           <button>
//             <h2>Edit</h2>
//           </button>
//           <NavbarToggler onClick={this.toggle} />
//           <Collapse isOpen={this.state.isOpen} navbar>
//             <Nav className="ml-auto" navbar>
//               <NavItem>
//                 <NavLink href="/components/">Saving Plan</NavLink>
//               </NavItem>
//               <NavItem>
//                 <NavLink href="/components/">Reports</NavLink>
//               </NavItem>
//               <UncontrolledDropdown nav inNavbar>
//                 <DropdownToggle nav caret>
//                   View
//                 </DropdownToggle>
//                 <DropdownMenu right>
//                   <DropdownItem>Monthly</DropdownItem>
//                   <DropdownItem>Weekly</DropdownItem>
//                   <DropdownItem divider />
//                   <DropdownItem>Yearly</DropdownItem>
//                 </DropdownMenu>
//               </UncontrolledDropdown>
//             </Nav>
//           </Collapse>
//         </Navbar>
//       </div>
//     );
//   }
// }
export default class Wallet extends React.Component {
  render() {
    const { summing, saving, calc } = this.props;

    return (
      <div>
        {" "}
        <div className="all">
          <h1 className="marios">
            Wallet{" "}
            <Badge color="secondary">
              {summing.map(user => {
                var sav = Math.trunc(Number(saving));
                var cal = Math.trunc(Number(calc));
                // var saving = parseInt( saving , 10);
                // var calc = parseInt( calc , 10);
                var sum = user.wallet + sav - cal;

                return sum < 0 ? (
                  <div>
                    <div className="neg">{sum}$</div>
                    <div className="sav">Change your plan</div>
                  </div>
                ) : (
                  <div>
                    {" "}
                    <div className="pos">{sum}$</div>
                    <div className="sav"> +{calc}$ (savings) </div>
                  </div>
                );
              })}
            </Badge>
          </h1>
        </div>
      </div>
    );
  }
}
