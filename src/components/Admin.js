import React, { Component } from "react";
import { Link } from "react-router-dom";
import Contact from "./Contact";
import { pause, makeRequestUrl } from "../utils";

const makeUrl = (path, params) =>
  makeRequestUrl(`http://localhost:8080/${path}`, params);

class Admin extends Component {
  state = {
    contacts_list: [],
    article_list: [],
    image_path: "",
    image_category: "",
    article_title: "",
    article_image: "",
    article_text: "",
    article_date: "",
    format: "",
    formats: "",
    token: null,
    nick: null,
    editMode: false
  };

  login = async (username, password) => {
    try {
      const url = makeUrl(`login`, {
        username,
        password,
        token: this.state.token
      });
      const response = await fetch(url);
      const answer = await response.json();
      if (answer.success) {
        const { token, nick } = answer.result;
        this.setState({ token, nick });
      } else {
        this.setState({ error_message: answer.message });
      }
    } catch (err) {
      this.setState({ error_message: err.message });
    }
  };
  logout = async token => {
    try {
      const url = makeUrl(`logout`, { token: this.state.token });
      const response = await fetch(url);
      const answer = await response.json();
      if (answer.success) {
        this.setState({ token: null, nick: null });
      } else {
        this.setState({ error_message: answer.message });
      }
    } catch (err) {
      this.setState({ error_message: err.message });
    }
  };

  onLoginSubmit = evt => {
    evt.preventDefault();
    const username = evt.target.username.value;
    const password = evt.target.password.value;
    if (!username) {
      return;
    }
    if (!password) {
      return;
    }
    this.login(username, password);
  };
  renderUser() {
    const { token } = this.state;
    if (token) {
      // user is logged in
      return this.renderUserLoggedIn();
    } else {
      return this.renderUserLoggedOut();
    }
  }
  renderUserLoggedOut() {
    return (
      <form className="third" onSubmit={this.onLoginSubmit}>
        <input name="username" placeholder="username" type="text" />
        <input name="password" placeholder="password" type="password" />
        <input type="submit" value="ok" />
      </form>
    );
  }
  renderUserLoggedIn() {
    const { nick } = this.state;
    return (
      <div>
        Hello, {nick}! <button onClick={this.logout}>logout</button>
      </div>
    );
  }

  onSubmit = evt => {
    // extract name and email from state
    const { image_path, image_category } = this.state;
    // create the contact from mail and email
    this.createContact({ image_path, image_category });

    // empty name and email so the text input fields are reset
    this.setState({ image_path: "", image_category: "", format: "" });
  };

  onArticleSubmit = evt => {
    const {
      article_title,
      article_image,
      article_text,
      article_date
    } = this.state;

    this.createArticle({
      article_title,
      article_image,
      article_text,
      article_date
    });

    this.setState({
      article_title: "",
      article_image: "",
      article_text: "",
      article_date: "",
      formats: ""
    });
  };
  createContact = async props => {
    try {
      if (!props || !(props.image_path && props.image_category)) {
        throw new Error();
      }
      const { image_path, image_category } = props;
      const response = await fetch(
        `http://localhost:8080/admin/new/?image_path=${image_path}&image_category=${image_category}`
      );
      const answer = await response.json();
      if (answer.success) {
        // we reproduce the user that was created in the database, locally
        const id = answer.result;
        const contact = { image_path, image_category, id };
        const contacts_list = [...this.state.contacts_list, contact];
        this.setState({ contacts_list });
      } else {
        this.setState({ error_message: answer.message });
      }
    } catch (err) {
      this.setState({ error_message: err.message });
    }
  };
  deleteContact = async id => {
    const message = this.state.contacts_list.map(msg => {
      if (msg.id == id) {
        return msg.image_path;
      }
    });
    var str = message.toString();
    var res = str.substr(9);

    const response = await fetch(`http://localhost:8080/admin/delete/${id}`);
    const answer = await response.json();
    if (answer.success) {
      // remove the user from the current list of users

      const contacts_list = this.state.contacts_list.filter(
        contact => contact.id !== id
      );

      this.setState({ contacts_list });
    } else {
      this.setState({ error_message: answer.message });
    }
  };

  getContactsList = async order => {
    const url = makeUrl(`admin/list`, { order, token: this.state.token });
    const response = await fetch(url);

    const answer = await response.json();
    if (answer.success) {
      const contacts_list = answer.result;
      this.setState({ contacts_list });
    } else {
      const error_message = answer.message;
      this.setState({ error_message });
    }
  };

  updateArticle = async (id, props) => {
    const { article_title, article_image, article_text, article_date } = props;
    const response = await fetch(
      `http://localhost:8080/admin/updateArticle/${id}?article_title=${article_title}&article_text=${article_text}&article_image=${article_image}&article_date=${article_date}`
    );
    const answer = await response.json();
    if (answer.success) {
      // we update the user, to reproduce the database changes:
      const article_list = this.state.article_list.map(contact => {
        // if this is the contact we need to change, update it. This will apply to exactly
        // one contact
        if (contact.id === id) {
          const new_contact = {
            id: contact.id,
            article_image: contact.article_image,
            article_date: contact.article_date,
            article_text: contact.article_text,
            article_title: contact.article_title
          };
          return new_contact;
        }
        // otherwise, don't change the contact at all
        else {
          return contact;
        }
      });
      this.setState({ article_list });
    } else {
      this.setState({ error_message: answer.message });
    }
  };

  deleteArticle = async id => {
    const response = await fetch(
      `http://localhost:8080/admin/deleteArticle/${id}`
    );
    const answer = await response.json();
    if (answer.success) {
      // remove the user from the current list of users

      const article_list = this.state.article_list.filter(
        contact => contact.id !== id
      );

      this.setState({ article_list });
    } else {
      this.setState({ error_message: answer.message });
    }
  };

  createArticle = async props => {
    const { article_title, article_image, article_text, article_date } = props;
    const response = await fetch(
      `http://localhost:8080/admin/newArticle?article_title=${article_title}&article_image=${article_image}&article_text=${article_text}&article_date=${article_date}`
    );
    const answer = await response.json();
    if (answer.success) {
      // we reproduce the user that was created in the database, locally
      const id = answer.result;
      const contact = {
        article_title,
        article_image,
        article_text,
        article_date,
        id
      };
      const article_list = [...this.state.article_list, contact];
      this.setState({ article_list });
    } else {
      this.setState({ error_message: answer.message });
    }
  };

  getArticlesList = async order => {
    const url = makeUrl(`admin/listArticle`, {
      order,
      token: this.state.token
    });
    const response = await fetch(url);

    const answer = await response.json();
    if (answer.success) {
      const article_list = answer.result;
      this.setState({ article_list });
    } else {
      const error_message = answer.message;
      this.setState({ error_message });
    }
  };

  componentDidMount() {
    this.getArticlesList();
    this.getContactsList();
  }

  render() {
    const { contacts_list } = this.state;
    const { article_list } = this.state;
    const { token } = this.state;
    return (
      <div>
        {this.renderUser()}

        {token ? (
          <div>
            <ul>
              <li>
                <Link to="/home">Home</Link>
              </li>
              <li>
                {" "}
                <Link to="/blog">Blog</Link>
              </li>
            </ul>
            <h2> Image Category</h2>
            <form className="third" onSubmit={this.onSubmit}>
              <select
                onChange={evt =>
                  this.setState({ image_category: evt.target.value })
                }
              >
                <option> -- select a Category -- </option>
                <option value="comics">comics</option>
                <option value="illustration">illustration</option>
                <option value="ads">ads</option>
                <option value="kids">kids</option>
              </select>
              <span>
                <input
                  type="file"
                  onChange={evt =>
                    this.setState({
                      image_path: `./images/${evt.target.files[0].name}`,
                      format: `${evt.target.value}`
                    })
                  }
                  value={this.state.format}
                />
              </span>

              <div>
                <input type="submit" value="ok" className="button" />
                <input type="reset" value="cancel" className="button" />
              </div>
            </form>

            <table className=" table table-bordered">
              <thead>
                <tr>
                  <th scope="col">Comics</th>
                  <th scope="col">kids</th>
                  <th scope="col">illustration</th>
                  <th scope="col">ads</th>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <td>
                    {contacts_list.map(contact =>
                      contact.image_category == "comics" ? (
                        <div key={contact.id}>
                          <button
                            onClick={() => {
                              if (
                                window.confirm(
                                  `Are you sure you wish to delete "${contact.image_path.substr(
                                    9
                                  )}" from your website ?`
                                )
                              )
                                this.deleteContact(contact.id);
                            }}
                          >
                            x
                          </button>

                          <img className="kk" src={contact.image_path} />
                        </div>
                      ) : null
                    )}
                  </td>

                  <td>
                    {contacts_list.map(contact =>
                      contact.image_category == "kids" ? (
                        <div key={contact.id}>
                          <button
                            onClick={() => {
                              if (
                                window.confirm(
                                  `Are you sure you wish to delete "${contact.image_path.substr(
                                    9
                                  )}" from your website ?`
                                )
                              )
                                this.deleteContact(contact.id);
                            }}
                          >
                            x
                          </button>

                          <img className="kk" src={contact.image_path} />
                        </div>
                      ) : null
                    )}
                  </td>

                  <td>
                    {contacts_list.map(contact =>
                      contact.image_category == "illustration" ? (
                        <div key={contact.id}>
                          <button
                            onClick={() => {
                              if (
                                window.confirm(
                                  `Are you sure you wish to delete "${contact.image_path.substr(
                                    9
                                  )}" from your website ?`
                                )
                              )
                                this.deleteContact(contact.id);
                            }}
                          >
                            x
                          </button>

                          <img className="kk" src={contact.image_path} />
                        </div>
                      ) : null
                    )}
                  </td>

                  <td>
                    {contacts_list.map(contact =>
                      contact.image_category == "ads" ? (
                        <div key={contact.id}>
                          <button
                            onClick={() => {
                              if (
                                window.confirm(
                                  `Are you sure you wish to delete "${contact.image_path.substr(
                                    9
                                  )}" from your website ?`
                                )
                              )
                                this.deleteContact(contact.id);
                            }}
                          >
                            x
                          </button>

                          <img className="kk" src={contact.image_path} />
                        </div>
                      ) : null
                    )}
                  </td>
                </tr>
              </tbody>
            </table>
            <div className="blog">
              <div>
                <h3>Blog Articles</h3>
              </div>
              <form onSubmit={this.onArticleSubmit}>
                <div className="title">
                  Title:{" "}
                  <input
                    type="text"
                    onChange={evt => {
                      this.setState({ article_title: evt.target.value });
                    }}
                    value={this.state.article_title}
                  />
                </div>
                <div className="desc">
                  <textarea
                    placeholder="Description"
                    type="text"
                    onChange={evt => {
                      this.setState({ article_text: evt.target.value });
                    }}
                    value={this.state.article_text}
                  >
                    {" "}
                    description:
                  </textarea>
                </div>

                <div className="date">
                  Date:{" "}
                  <input
                    type="date"
                    onChange={evt => {
                      this.setState({ article_date: evt.target.value });
                    }}
                    value={this.state.article_date}
                  />
                </div>
                <div className="file">
                  <input
                    type="file"
                    onChange={evt =>
                      this.setState({
                        article_image: `./images/${evt.target.files[0].name}`,
                        formats: `${evt.target.value}`
                      })
                    }
                    value={this.state.formats}
                  />
                </div>
                <div>
                  <input type="submit" value="ok" className="button" />
                  <input type="reset" value="cancel" className="button" />
                </div>
              </form>
            </div>
            <div className="container">
              <div className="row">
                <table className="sturdy">
                  <thead>
                    <tr>
                      <th scope="col">Title</th>
                      <th scope="col">Image</th>
                      <th scope="col">Description</th>
                      <th scope="col">Date</th>
                      <th scope="col">Edit/Delete</th>
                    </tr>
                  </thead>
                  {article_list.map(contact => {
                    return (
                      <Contact
                        key={contact.id}
                        id={contact.id}
                        article_text={contact.article_text}
                        article_title={contact.article_title}
                        article_image={contact.article_image}
                        article_date={contact.article_date}
                        updateArticle={this.updateArticle}
                        deleteArticle={this.deleteArticle}
                        formats={contact.formats}
                      />
                    );
                  })}
                </table>
              </div>
            </div>
          </div>
        ) : null}
      </div>
    );
  }
}
export default Admin;
