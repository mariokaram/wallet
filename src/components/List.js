import React from "react";
import { Table } from "reactstrap";

class List extends React.Component {
  render() {
    const { income, expense, summing, sumExpense } = this.props;

    return (
      <div>
        <Table hover>
          <thead>
            <tr>
              <th>Type</th>
              <th>Amount: $</th>
              <th>Start Date</th>

              <th>Description</th>
              <th>Category</th>
            </tr>
          </thead>
          {summing.map(price => (
            <strong>Incomes total: {price.INCOME} $</strong>
          ))}
          <tbody>
            {income.map(item => (
              <tr>
                <td>
                  <div>{item.type}</div>
                </td>

                <td>
                  <div>{item.amount}</div>
                </td>

                <td>
                  <div>{item.start_date}</div>
                </td>

                <td>
                  <div>{item.description}</div>
                </td>

                <td>
                  <strong>{item.option}</strong>
                </td>
              </tr>
            ))}
            <br />
            {sumExpense.map(price => (
              <strong>Expenses total: {price.EXPENSE} $</strong>
            ))}
            {expense.map(item => (
              <tr>
                <td>
                  <div>{item.type}</div>
                </td>

                <td>
                  <div>{item.amount}</div>
                </td>

                <td>
                  <div>{item.start_date}</div>
                </td>

                <td>
                  <div>{item.description}</div>
                </td>

                <td>
                  <strong>{item.option}</strong>
                </td>
              </tr>
            ))}
          </tbody>
        </Table>
      </div>
    );
  }
}
export default List;
