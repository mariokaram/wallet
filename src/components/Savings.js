import React, { Component } from "react";

import { Button, Form, FormGroup, Label, Input, FormText } from "reactstrap";

class Savings extends Component {
  render() {
    return (
      <Form>
        <FormGroup>
          <Label for="exampleEmail">Save Money</Label>
          <Input
            type="number"
            name="number"
            id="saving"
            placeholder="How much do you want to save ?"
          />
        </FormGroup>

        <FormGroup>
          <Label for="exampleSelect">How long</Label>
          <Input type="select" name="select" id="exampleSelect">
            <option>1 Month</option>
            <option>6 Months</option>
            <option>1 Year</option>
          </Input>
        </FormGroup>
        <Button>Submit</Button>
      </Form>
    );
  }
}

export default Savings;
