import React, { Component } from "react";
import "./App.css";
import { withRouter, Switch, Route, Link } from "react-router-dom";
import { makeRequestUrl } from "./utils.js";
import * as auth0Client from "./auth";
import { ToastContainer, toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import Header from "./components/Header";
import Header2 from "./components/Header2";
import Wallet from "./components/Wallet";
import Wallet2 from "./components/Wallet2";
import List from "./components/List";
import Savings from "./components/Savings";
import Safest from "./components/Safest";
import IfAuthenticated from "./IfAuthenticated";
import SecuredRoute from "./SecuredRoute";

const makeUrl = (path, params) =>
  makeRequestUrl(`http://localhost:8080/${path}`, params);

class App extends Component {
  state = {
    wallet_list: [],
    error_message: "",
    isLoading: false,
    checkingSession: true,
    sum: [],
    type: "",
    amount: null,
    start_date: "",
    end_date: "",
    repeat: "",
    currency: "",
    description: "",
    option: "",
    income: "",
    expense: "",
    sumExpense: [],
    plan: "",
    saving: [],
    calc: null,
    Rec_expense: null,
    Rec_income: null,
    Fixed_expense: null,
    Fixed_income: null
  };
  async componentDidMount() {
    this.Rec_expense();
    this.Fixed_expense();
    this.Fixed_income();
    this.Rec_income();
    this.plan();
    this.saving();
    this.byTypeIncome();
    this.byTypeExpense();
    this.sum();
    this.sumExpense();
    this.getContactsList();
    if (this.props.location.pathname === "/callback") {
      this.setState({ checkingSession: false });
      return;
    }
    try {
      await auth0Client.silentAuth();
      await this.getPersonalPageData();
      this.forceUpdate();
    } catch (err) {
      if (err.error !== "login_required") {
        console.log(err.error);
      }
    }
    this.setState({ checkingSession: false });
  }

  getContact = async id => {
    // check if we already have the contact
    const previous_contact = this.state.wallet_list.find(
      contact => contact.id === id
    );
    if (previous_contact) {
      return; // do nothing, no need to reload a contact we already have
    }
    try {
      const url = makeUrl(`admin/get/${id}`);
      const response = await fetch(url, {
        headers: { Authorization: `Bearer ${auth0Client.getIdToken()}` }
      });
      const answer = await response.json();
      if (answer.success) {
        // add the user to the current list of contacts
        const contact = answer.result;
        const wallet_list = [...this.state.wallet_list, contact];
        this.setState({ wallet_list });
        toast(`contact loaded`);
      } else {
        this.setState({ error_message: answer.message });
        toast.error(answer.message);
      }
    } catch (err) {
      this.setState({ error_message: err.message });
      toast.error(err.message);
    }
  };

  createContact = async props => {
    try {
      const { type, description, amount, start_date, currency, option } = props;
      const url = makeUrl(`admin/new`, {
        type
      });
      const response = await fetch(url, {
        headers: { Authorization: `Bearer ${auth0Client.getIdToken()}` }
      });
      const answer = await response.json();
      if (answer.success) {
        // we reproduce the user that was created in the database, locally
        const id = answer.result;
        const contact = {
          type,

          id
        };
        const wallet_list = [...this.state.wallet_list, contact];
        this.setState({ wallet_list });
      } else {
        this.setState({ error_message: answer.message });
        toast.error(answer.message);
      }
    } catch (err) {
      this.setState({ error_message: err.message });
      toast.error(err.message);
    }
  };

  Rec_income = async () => {
    try {
      const response = await fetch(`http://localhost:8080/admin/Rec_income`);
      const answer = await response.json();
      if (answer.success) {
        const Rec_income = answer.result;
        this.setState({ Rec_income });
      } else {
        this.setState({ error_message: answer.message });
      }
    } catch (err) {
      this.setState({ error_message: err.message });
    }
  };

  Rec_expense = async () => {
    try {
      const response = await fetch(`http://localhost:8080/admin/Rec_expense`);
      const answer = await response.json();
      if (answer.success) {
        const Rec_expense = answer.result;
        this.setState({ Rec_expense });
      } else {
        this.setState({ error_message: answer.message });
      }
    } catch (err) {
      this.setState({ error_message: err.message });
    }
  };

  Fixed_income = async () => {
    try {
      const response = await fetch(`http://localhost:8080/admin/Fixed_income`);
      const answer = await response.json();
      if (answer.success) {
        const Fixed_income = answer.result;
        this.setState({ Fixed_income });
      } else {
        this.setState({ error_message: answer.message });
      }
    } catch (err) {
      this.setState({ error_message: err.message });
    }
  };

  Fixed_expense = async () => {
    try {
      const response = await fetch(`http://localhost:8080/admin/Fixed_expense`);
      const answer = await response.json();
      if (answer.success) {
        const Fixed_expense = answer.result;
        this.setState({ Fixed_expense });
      } else {
        this.setState({ error_message: answer.message });
      }
    } catch (err) {
      this.setState({ error_message: err.message });
    }
  };

  sum = async () => {
    try {
      const response = await fetch(`http://localhost:8080/admin/sum`);
      const answer = await response.json();
      if (answer.success) {
        const sum = answer.result;
        this.setState({ sum });
      } else {
        this.setState({ error_message: answer.message });
      }
    } catch (err) {
      this.setState({ error_message: err.message });
    }
  };

  sumExpense = async () => {
    try {
      const response = await fetch(`http://localhost:8080/admin/sumExpense`);
      const answer = await response.json();
      if (answer.success) {
        const sumExpense = answer.result;
        this.setState({ sumExpense });
      } else {
        this.setState({ error_message: answer.message });
      }
    } catch (err) {
      this.setState({ error_message: err.message });
    }
  };

  byTypeIncome = async type => {
    try {
      const { types } = this.state;
      const response = await fetch(`http://localhost:8080/admin/typeIncome`);

      const answer = await response.json();
      if (answer.success) {
        const income = answer.result;
        this.setState({ income });
      } else {
        this.setState({ error_message: answer.message });
      }
    } catch (err) {
      this.setState({ error_message: err.message });
    }
  };

  plan = async () => {
    try {
      const response = await fetch(`http://localhost:8080/admin/plan`);

      const answer = await response.json();
      if (answer.success) {
        const plan = answer.result;
        this.setState({ plan });
      } else {
        this.setState({ error_message: answer.message });
      }
    } catch (err) {
      this.setState({ error_message: err.message });
    }
  };

  saving = async () => {
    try {
      const response = await fetch(`http://localhost:8080/admin/saving`);

      const answer = await response.json();
      if (answer.success) {
        const saving = answer.result;
        this.setState({ saving });
      } else {
        this.setState({ error_message: answer.message });
      }
    } catch (err) {
      this.setState({ error_message: err.message });
    }
  };

  byTypeExpense = async type => {
    try {
      const { types } = this.state;
      const response = await fetch(`http://localhost:8080/admin/typeExpense`);

      const answer = await response.json();
      if (answer.success) {
        const expense = answer.result;
        this.setState({ expense });
      } else {
        this.setState({ error_message: answer.message });
      }
    } catch (err) {
      this.setState({ error_message: err.message });
    }
  };

  getContactsList = async order => {
    try {
      const url = makeUrl(`admin/listContact`, { order });

      const response = await fetch(url);

      const answer = await response.json();
      if (answer) {
        const wallet_list = answer.result;
        this.setState({ wallet_list });
      } else {
        const error_message = answer.message;
        this.setState({ error_message });
      }
    } catch (err) {
      this.setState({ error_message: err.message });
    }
  };
  getPersonalPageData = async () => {
    try {
      const url = makeUrl(`mypage`);
      const response = await fetch(url, {
        headers: { Authorization: `Bearer ${auth0Client.getIdToken()}` }
      });
      const answer = await response.json();
      if (answer.success) {
        const message = answer.result;
        toast(`received from the server: '${message}'`);
      } else {
        this.setState({ error_message: answer.message });
        toast.error(
          `error message received from the server: ${answer.message}`
        );
      }
    } catch (err) {
      this.setState({ error_message: err.message });
      toast.error(err.message);
    }
  };

  renderUser() {
    const isLoggedIn = auth0Client.isAuthenticated();
    if (isLoggedIn) {
      return this.renderUserLoggedIn();
    } else {
      return this.renderUserLoggedOut();
    }
  }
  renderUserLoggedOut() {
    return (
      <div>
        <nav class="navbar navbar-light bg-dark">
          <div class="navbar-brand mx-auto">
            <h1 className="titles">You will be amazed! </h1>
            <button
              class="btn btn-primary"
              type="submit"
              onClick={auth0Client.signIn}
            >
              Sign In
            </button>
          </div>
        </nav>
        <img className="bgh" src="../images/money.jpg" />
      </div>
    );
  }

  renderCreateForm = () => {
    return (
      <form className="third" onSubmit={this.onSubmit}>
        <input
          type="text"
          placeholder="name"
          onChange={evt => this.setState({ type: evt.target.value })}
          value={this.state.type}
        />
        <div>
          <input type="submit" value="ok" />
          <input type="reset" value="cancel" className="button" />
        </div>
      </form>
    );
  };

  onSubmit = evt => {
    // stop the form from submitting:
    evt.preventDefault();
    // extract name and email from state
    const { type } = this.state;
    // create the contact from mail and email
    this.createContact({ type });
    // empty name and email so the text input fields are reset
    this.setState({ type: "" });
  };

  Calc = amount => {
    const { saving } = this.state;
    var mar = "";
    saving.map(duration => {
      if (duration.repeat == "monthly") {
        {
          mar = amount;
        }
      }
      if (duration.repeat == "one year") {
        {
          mar = amount / 12;
        }
      }
      if (duration.repeat == "six months") {
        {
          mar = amount / 6;
        }
      }
    });
    return mar;
  };

  renderUserLoggedIn() {
    const nick = auth0Client.getProfile().name;

    return (
      <div>
        <nav class="navbar navbar-light bg-dark">
          <div>
            <button
              class=" btn btn-primary abc "
              type="submit"
              onClick={() => {
                auth0Client.signOut();
                this.setState({});
              }}
            >
              Hello! {nick} -> Logout
            </button>
          </div>

          <div class="navbar-brand mx-auto  ">
            {" "}
            <Link to="/savings">Saving Plan</Link>
          </div>

          <div class="navbar-brand mx-auto ">
            {" "}
            <Link to="/Add">Add data</Link>
          </div>

          <div>
            <Header
              summing={this.state.plan}
              calc={this.state.saving.map(amount =>
                Math.trunc(this.Calc(amount.amount))
              )}
              saving={this.state.saving.map(amount => amount.amount)}
            />
          </div>
        </nav>
        <List
          summing={this.state.sum}
          income={this.state.income}
          expense={this.state.expense}
          sumExpense={this.state.sumExpense}
        />
        <br />
        <br />
        {/* <div className="saving">
          <strong>
            Safest plan:
            <Safest
              income={this.state.income}
              expense={this.state.expense}
              saving={this.state.saving.map(amount => amount.amount)}
              Rec_expense={this.state.Rec_expense}
              Rec_income={this.state.Rec_income}
              Fixed_expense={this.state.Fixed_expense}
              Fixed_income={this.state.Fixed_income}
            />
          </strong>
        </div> */}
        <div className="saving">
          {this.state.saving.map(amount =>
            this.state.Rec_income[0].Rec_income -
              this.state.Rec_expense[0].Rec_expense >
            this.Calc(amount.amount) ? (
              <div>
                <strong>Saving plan: From {amount.start_date}</strong> You
                should save{" "}
                <strong>
                  {this.state.saving.map(amount =>
                    Math.trunc(this.Calc(amount.amount))
                  )}
                  $/per month {""}
                </strong>
                in order to get your{" "}
                <strong>
                  {this.state.saving.map(date => {
                    return date.amount;
                  })}{" "}
                  ${" "}
                </strong>
                after{" "}
                <strong>
                  {this.state.saving.map(date => {
                    return date.repeat;
                  })}{" "}
                </strong>
              </div>
            ) : (
              <strong>
                It is not doable to save this plan, Please change your plan!
              </strong>
            )
          )}
        </div>

        <IfAuthenticated>
          <Link to="/create">create</Link>
        </IfAuthenticated>
      </div>
    );
  }
  home = () => {
    return (
      <div>
        <nav class="navbar navbar-light bg-dark">
          <div class="navbar-brand mx-auto">
            <h1 className="titles">Wallet Savings </h1>

            <Link to="/profile">
              <button class="btn btn-primary" type="submit">
                Start now
              </button>
            </Link>
          </div>
        </nav>

        <img className="bgh" src="../images/money.jpg" />
      </div>
    );
  };

  renderProfilePage = () => {
    if (this.state.checkingSession) {
      return <p>validating session...</p>;
    }
    return <div>{this.renderUser()}</div>;
  };

  renderContent() {
    if (this.state.isLoading) {
      return <p>loading...</p>;
    }

    return (
      <Switch>
        <Route path="/" exact render={this.home} />
        <SecuredRoute path="/create" render={this.renderCreateForm} />

        <Route path="/profile" exact render={this.renderProfilePage} />
        <Route path="/savings" component={Savings} />
        <Route path="/callback" render={this.handleAuthentication} />
        <Route render={() => <div>not found!</div>} />
      </Switch>
    );
  }

  isLogging = false;
  login = async () => {
    if (this.isLogging === true) {
      return;
    }
    this.isLogging = true;
    try {
      await auth0Client.handleAuthentication();
      const name = auth0Client.getProfile().name;
      await this.getPersonalPageData();
      toast(`${name} is logged in`);
      this.props.history.push("/profile");
    } catch (err) {
      this.isLogging = false;
      toast.error(err.message);
    }
  };
  handleAuthentication = ({ history }) => {
    this.login(history);
    return <p>wait...</p>;
  };

  render() {
    return (
      <div className="App">
        {this.renderContent()}
        <ToastContainer />
      </div>
    );
  }
}

export default withRouter(App);
